# Portainer

Traefik can be accessed via [traefik.klasta.local](https://traefik.klasta.local).
For this to work add the following line to your `/etc/hosts` file.
```
192.168.10.2 traefik.klasta.local
```
