# Portainer

Portainer can be accessed via [portainer.klasta.local](https://portainer.klasta.local).
For this to work add the following line to your `/etc/hosts` file.
```
192.168.10.2 portainer.klasta.local
```
