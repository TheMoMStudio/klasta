# Klasta Storage

Network storage is provided via [glusterfs](https://www.gluster.org/).

## Glusterfs Commands

### Show volume status

```
sudo gluster volume status
```

### Restart gluster volume

```
sudo gluster volume stop gv0
sudo gluster volume start gv0
```

### Restart glusterd

```
sudo service glusterd stop
sudo service glusterd start
```

### Mount /data manually

```
sudo mount /data
```

### Reset glusterd (in case everything broke)

```
sudo systemctl stop glusterd
sudo rm -rf /var/lib/glusterd/*
sudo systemctl start glusterd

```
