# Swarm

[Docker Swarm](https://docs.docker.com/engine/swarm/) is used to deploy services. All deployments are listed in the [deployments](../../deployments) folder.
