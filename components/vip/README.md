# Virtual IP Address

[Keepalived](https://www.keepalived.org/manpage.html) is used for automatic
IP-Address failover on the manager nodes.

## Required Variables

Same for all nodes:
- `vip_ip` (e.g. `192.168.10.2`)
- `vip_pw` (e.g. `12345`)

Different for all nodes
- `vip_priority` (e.g. `255`)
