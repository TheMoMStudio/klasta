# Router

Device: TP Link TL-WR1043ND v2
Mor information: https://openwrt.org/toh/tp-link/tl-wr1043nd

## Intallation

1. Install [openwrt](https://openwrt.org/toh/tp-link/tl-wr1043nd) on the router

2. Configure admin password via web interface (from authorized_keys.yaml)

3. Change Lan IP Address to 192.168.10.1

4. Add ssh key via web interface (one is enough, the others get added via ansible)

5. Add USB storage to extend small internal memory of 2MB. Instructions taken from https://openwrt.org/docs/guide-user/additional-software/extroot_configuration

```
opkg update
opkg install block-mount kmod-fs-ext4 e2fsprogs parted kmod-usb-storage
parted -s /dev/sda -- mklabel gpt mkpart extroot 2048s -2048s

DEVICE="$(sed -n -e "/\s\/overlay\s.*$/s///p" /etc/mtab)"
uci -q delete fstab.rwm
uci set fstab.rwm="mount"
uci set fstab.rwm.device="${DEVICE}"
uci set fstab.rwm.target="/rwm"
uci commit fstab

DEVICE="/dev/sda1"
mkfs.ext4 -L extroot ${DEVICE}

eval $(block info ${DEVICE} | grep -o -e "UUID=\S*")
uci -q delete fstab.overlay
uci set fstab.overlay="mount"
uci set fstab.overlay.uuid="${UUID}"
uci set fstab.overlay.target="/overlay"
uci commit fstab

mount ${DEVICE} /mnt
tar -C /overlay -cvf - . | tar -C /mnt -xf -
reboot
```

6. Install openssh-sftp-server, needed for ansible to work
See: https://github.com/gekmihesg/ansible-openwrt/issues/18

```
ssh root@192.168.10.1
opkg update && opkg install openssh-sftp-server
```

7. Run ansible scripts to install some more packages and setup configuration

```
make setup-router
```

8. Workaround until NN708/ansible-openwrt features are added

```
ssh root@192.168.10.1
uci set ddns.myddns_ipv4.use_https='1'
uci set ddns.myddns_ipv4.cacerts='/etc/ssl/certs/ca-certificates.crt'
uci commit
/etc/init.d/ddns reload
```

## Wireguard

### Adding a new client

1. Open the ansible secrets and add a new peer configuration.
2. Run `make setup-router`
3. Restart the `vpn` (wireguard) interface via the web interface.

### Client Configuration

#### Nix Example

```nix
networking.wireguard.interfaces = {
  # "wg0" is the network interface name. You can name the interface arbitrarily.
  wg0 = {
    # Determines the IP address and subnet of the client's end of the tunnel interface.
    ips = [ "192.168.9.</24" ];
    listenPort = 6785; # to match firewall allowedUDPPorts (without this wg uses random port numbers)

    # Path to the private key file.
    #
    # Note: The private key can also be included inline via the privateKey option,
    # but this makes the private key world-readable; thus, using privateKeyFile is
    # recommended.
    privateKeyFile = "<path to your private key>";

    peers = [
      # For a client configuration, one peer entry for the server will suffice.

      {
        # Public key of the server (not a file path).
        publicKey = "<server public key>";

        # Or forward only particular subnets
        allowedIPs = [ "192.168.10.0/24" "192.168.9.0/24" ];

        # Set this to the server IP and port.
        endpoint = "<server ip or domain>:6785";

        # Send keepalives every 25 seconds. Important to keep NAT tables alive.
        persistentKeepalive = 25;
      }
    ];
  };
};
networking.firewall.allowedUDPPorts = [ 6785 ];
```
