#!/bin/sh

uhubctl -l 2 -a 1 -p 2
sleep 2 # Wait 2 seconds for the disk to power on
kopia snapshot create /data
uhubctl -l 2 -a 0 -p 2
