# Backup

Backups are done using [Kopia](https://kopia.io/docs/)

## Restore Backup

1. Power on backup disk
```
sudo uhubctl -l 2 -a 1 -p 2
```

2. List snapshots
```
sudo kopia snapshot list
```

3. Restore backup
```
sudo kopia snapshot restore <kopia backup id> /data/
```

4. Power off backup disk
```
sudo uhubctl -l 2 -a 0 -p 2
```
