# Klasta [WIP]

The cluster consists of multiple raspbbery pi. The cluster can run docker containers using [docker swarm](https://docs.docker.com/engine/swarm/). Persistent storage is managed with [glusterfs](https://www.gluster.org/).
A raspbbery pi is either a `manager` or `worker` node. All `manager` nodes are `docker swarm` managers. Replicated storage is setup on all `manager`, which gets mounted on the `worker` nodes.

## Nodes

- `rpia`: role=`manager` model=`Raspberry Pi 4 8Gb`
- `rpib`: role=`manager` model=`Raspberry Pi 4 4Gb`
- `rpic`: role=`manager` model=`Raspberry Pi 3B+ 1GB`
- `rpid`: role=`worker` model`Raspberry Pi 3B+ 1GB` __NOT CONNECTED YET__
- `rpie`: role=`worker` model=`Raspberry Pi 3B+ 1GB` __NOT CONNECTED YET__

## How To

### Setup Raspberry Pi

1. Download the [Raspberry Pi Os Lite Image](https://www.raspberrypi.com/software/operating-systems/)
2. Flash the image to the sd card
3. Enable ssh (create an empty file named `ssh` in the boot folder
4. Set gpu mem in `boot/config.txt`

```
...
[all]
gpu_mem=16
```

5. Enable container features
   Edit `/boot/cmdline.txt` and add the following to the end of the line:

```
cgroup_enable=cpuset cgroup_memory=1 cgroup_enable=memory
```

6. Set hostname in `rootfs/etc/hostname`
7. Insert the ssd card and start the raspberry pi
8. ssh pi@<HOSTNAME> (password raspbbery)
9. Run the setup script `sudo raspi-config`
10. [Configure static ip](https://pimylifeup.com/raspberry-pi-static-ip-address/) (nececcary only for master)
11. Reboot

### Setup Cluster

`make setup-cluster`

This will:

- install apt packages for docker
- init docker swarm cluster
- setup docker networks
- make worker nodes join automatically
- setup nfs storage
- setup backup with kopia

### Update Cluster

`make update-cluster`

This will:

- update apt packages
- make worker nodes join automatically
- update nfs storage
- update backup with kopia

### Deploy Cluster

`make deploy-cluster`

This will:

- deploy all deployments in deployments/

### Update Secrets

Secrets are managed via [ansible vault](https://docs.ansible.com/ansible/latest/user_guide/vault.html) and stored in [vars_secret.yaml](./vars_secret.yaml).

To view the secrets:
```
ansible-vault view --vault-password-file vars_secret vars_secret.yaml
```
To edit the secrets:
```
ansible-vault edit --vault-password-file vars_secret vars_secret.yaml
```
