ansible-deps:
	ansible-galaxy install gekmihesg.openwrt
	ansible-galaxy collection install nn708.openwrt

ping:
	ansible-playbook components/pi/ping.yaml

boot:
	ansible-playbook components/pi/boot.yaml

setup-cluster: setup-pi setup-backup setup-storage setup-swarm setup-vip

setup-pi:
	ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret components/pi/setup.yaml
setup-swarm:
	ansible-playbook components/swarm/setup_1.yaml
	ssh pi@192.168.10.2 'docker network rm ingress' # This step needs to be manual, to be able to confirm the prompt
	ansible-playbook components/swarm/setup_2.yaml
setup-backup:
	# ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret components/backup/setup.yaml
setup-storage:
	ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret components/storage/setup.yaml
setup-vip:
	ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret components/vip/setup.yaml
setup-router:
	ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret components/router/setup.yaml


update-cluster: update-pi update-backup update-storage update-swarm update-vip

update-pi:
	ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret components/pi/update.yaml
update-swarm:
	ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret components/swarm/update.yaml
update-backup:
	# ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret components/backup/update.yaml
update-storage:
	ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret components/storage/update.yaml
update-vip:
	ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret components/vip/setup.yaml


deploy-all: deploy-duckdns deploy-portainer deploy-traefik deploy-eintopf-test deploy-swzpln klasse-methode-code

deploy-duckdns:
	ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret deployments/duckdns/deploy.yaml
deploy-eintopf-test:
	ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret deployments/eintopf-test/deploy.yaml
deploy-portainer:
	ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret deployments/portainer/deploy.yaml
deploy-logging:
	ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret deployments/logging/deploy.yaml
deploy-swzpln:
	ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret deployments/swzpln/deploy.yaml
deploy-themom-ci:
	ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret deployments/themom-ci/deploy.yaml
deploy-klasse-methode-code:
	ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret deployments/klasse-methode-code/deploy.yaml
deploy-traefik:
	ansible-playbook --extra-vars @vars_secret.yaml --vault-password-file vars_secret deployments/traefik/deploy.yaml
