# Networking

## Setup

### Klasta Router Configuration

See [../components/router/README.md](../components/router/README.md)

### Home Router Portforwarding

The following port forwardings have to be configured in the home router pointing to `klasta-router`:

- 80:80
- 443:443
- 9873:9873

### Virtual IP

See [../components/vip/README.md](../components/vip/README.md)
